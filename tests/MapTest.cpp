/* 
 * File:   MapTest.cpp
 * Author: Workstation
 *
 * Created on 07 Nov 2014, 10:09:32 PM
 */

#include <iostream>
#include <set>
#include "Test.h"
#include "LinkedHashMap.h"

/*
 * Simple C++ Test Suite
 */
using namespace std;

void testRemove() {
    LinkedHashMap<string, int> map(5);
    map.insert("Lars", 1);
    map.insert("Günther", 12);
    map.insert("Max", 2);
    REQUIRE(map.size() == 3);

    REQUIRE(map.getOrDefault("Markus", -1) == -1);
    REQUIRE(map.get("Lars") == 1);
    REQUIRE(map.get("Max") == 2);
    REQUIRE(map.get("Günther") == 12);

    REQUIRE(map.remove("Max"));

    REQUIRE(map.getOrDefault("Markus", -1) == -1);
    REQUIRE(map.get("Lars") == 1);
    REQUIRE(map.getOrDefault("Max", -1) == -1);
    REQUIRE(map.get("Günther") == 12);

    REQUIRE(map.remove("Lars"));
    REQUIRE(!map.remove("Lars"));


    REQUIRE(map.getOrDefault("Markus", -1) == -1);
    REQUIRE(map.getOrDefault("Lars", -1) == -1);
    REQUIRE(map.getOrDefault("Max", -1) == -1);
    REQUIRE(map.get("Günther") == 12);

    map.insert("Lars", 1);
    map.insert("Günther", 12);
    map.insert("Max", 2);
    REQUIRE(map.remove("Günther"));
}

void testIterate() {

    vector<int> vec;
    LinkedHashMap<int, int> map(5);
    map.insert(-100, 1);
    map.insert(0, 12);
    map.insert(200, 2);
    REQUIRE(map.size() == 3);

    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == 200);
    REQUIRE(vec[1] == 0);
    REQUIRE(vec[2] == -100);

    map.remove(0);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == 200);
    REQUIRE(vec[1] == -100);

    map.insert(0, 11);

    REQUIRE(map.get(0) == 11);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == 0);
    REQUIRE(vec[1] == 200);
    REQUIRE(vec[2] == -100);

    map.insert(-100, -11);
    REQUIRE(map.size() == 3);
    REQUIRE(map.get(-100) == -11);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == -100);
    REQUIRE(vec[1] == 0);
    REQUIRE(vec[2] == 200);

}

void testOrder() {

    vector<string> vec;
    LinkedHashMap<string, int> map(5);
    map.insert("Lars", 1);
    map.insert("Günther", 12);
    map.insert("Max", 2);
    REQUIRE(map.size() == 3);

    for (auto i = map.begin(); i != map.end(); ++i) {
        vec.push_back(*i);
    }

    REQUIRE(vec[0] == "Max");
    REQUIRE(vec[1] == "Günther");
    REQUIRE(vec[2] == "Lars");

    map.remove("Günther");

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == "Max");
    REQUIRE(vec[1] == "Lars");

    map.insert("Günther", 11);

    REQUIRE(map.get("Günther") == 11);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == "Günther");
    REQUIRE(vec[1] == "Max");
    REQUIRE(vec[2] == "Lars");

    map.insert("Lars", -11);
    REQUIRE(map.size() == 3);
    REQUIRE(map.get("Lars") == -11);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == "Lars");
    REQUIRE(vec[1] == "Günther");
    REQUIRE(vec[2] == "Max");

}

void testSize() {

    vector<string> vec;
    LinkedHashMap<string, int> map(2);
    map.insert("Lars", 1);
    map.insert("Günther", 12);
    map.insert("Max", 2);
    REQUIRE(map.size() == 2);

    map.clear();
    REQUIRE(map.size() == 0);

    map.insert("Lars", 1);
    map.insert("Günther", 12);
    map.insert("Max", 2);

    for (auto i : map) {
        vec.push_back(i);
    }

    REQUIRE(vec[0] == "Max");
    REQUIRE(vec[1] == "Günther");

    map.remove("Günther");
    REQUIRE(map.size() == 1);

    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }
    REQUIRE(vec[0] == "Max");

    map.insert("Bob", 11);
    map.insert("Clive", 111);
    REQUIRE(map.size() == 2);
    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }
    REQUIRE(vec[0] == "Clive");
    REQUIRE(vec[1] == "Bob");

    map.insert("Bob", 120);
    REQUIRE(map.get("Bob") == 120);
    REQUIRE(map.size() == 2);
    vec.clear();
    for (auto i : map) {
        vec.push_back(i);
    }
    REQUIRE(vec[0] == "Bob");
    REQUIRE(vec[1] == "Clive");


    map.remove("Bob");
    map.remove("Clive");

    REQUIRE(map.size() == 0);

}

void testMB() {

    LinkedHashMap<int, int> map(2000_b);
    int N = sizeof (int) * 3;
    REQUIRE(map.size() < 2000 / N);
}

void continuousTest() {

    {
        LinkedHashMap<uint64_t, std::string> map(2000);

        for (int i = 0; i < 100000; ++i) {
            REQUIRE(map.size() <= map.capacity(), "Size of map = " + std::to_string(map.size()) +
                    " capacity = " + std::to_string(map.capacity()));
            map.insert(i, "Howdee");
        }
        REQUIRE(map.size() <= map.capacity());
    }

    {
        LinkedHashMap<uint64_t, std::string> map(2000);

        for (int i = 0; i < 100000; ++i) {
            REQUIRE(map.size() <= map.capacity(), "Size of map = " + std::to_string(map.size()) +
                    " capacity = " + std::to_string(map.capacity()));
            map.insert(i, "Howdee");
        }
        REQUIRE(map.size() <= map.capacity());
    }
}

int main(int argc, char** argv) {

    Test::startSuite("FHM");
    Test::testFunction("Delete", testRemove);
    Test::testFunction("Iterate", testIterate);
    Test::testFunction("Order", testOrder);
    Test::testFunction("FixedSize", testSize);
    Test::testFunction("Bytes", testMB);
    Test::testFunction("TestMemleak", continuousTest);
    Test::endSuite();

    return 0;
}

