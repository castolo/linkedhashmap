/* 
 * File:   FixedHashMap.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * A class that represents a fixed-size hash map. When a new entry is added to the
 * map that would exceed its allowable size, the oldest entry is removed from the
 * map.
 * 
 * Created on 07 November 2014, 3:44 PM
 */

#ifndef LINKEDHASHMAP_H
#define	LINKEDHASHMAP_H

#include <unordered_map>
#include <cstdint>
#include <functional>
#include <iterator>
#include <list>

// <editor-fold defaultstate="collapsed" desc="Byte Literal">

/**
 * User-defined literal for Byte
 */
class Byte {
public:

    explicit constexpr Byte(uint32_t val) : mVal(val) {
    }

    inline uint32_t val() const {
        return mVal;
    }
private:
    uint32_t mVal;

};
// </editor-fold>

constexpr Byte operator "" _b(unsigned long long val) {
    return Byte{static_cast<uint32_t> (val)};
}

/**
 * A class that represents a fixed-size hash map. When a new entry is added to the
 * map that would exceed its allowable size, the oldest entry is removed from the
 * map.
 */
template <typename K, typename V>
class LinkedHashMap {
    class Iterator;
    class ConstIterator;

    struct Entry {
        V value;
        typename std::list<K>::iterator iterator;

    };

public:

    /**
     * Creates a new fixed-size map
     * @param maxSize
     */
    LinkedHashMap(int maxSize) : MAX_SIZE(maxSize) {
        map.reserve(MAX_SIZE);
    }

    /**
     * Creates a map that does not exceed the given <b>number of bytes</b> in memory 
     */
    LinkedHashMap(Byte numBytes) :
    LinkedHashMap(static_cast<int> ((numBytes.val() - sizeof (int)) / (2 * sizeof (K) + sizeof (Entry)))) {
    }

    /**
     *  Erases all elements in an %unordered_map.
     *  Note that this function only erases the elements, and that if the
     *  elements themselves are pointers, the pointed-to memory is not touched
     *  in any way.  Managing the pointer is the user's responsibility.
     */
    inline void clear() {
        map.clear();
        keys.clear();
    }

    /**
     * Determines if the given key is stored in the hash map
     * @param key the key
     * @return whether the key is in the map
     */
    inline bool contains(K key) const {
        return map.find(key) != map.end();
    }

    /**
     * Returns the size of the map 
     */
    inline size_t size() const {
        return map.size();
    }

    /**
     * Returns the total number of elements that the map can hold
     */
    inline int capacity() const {
        return MAX_SIZE;
    }

    /**
     * Returns true if the map is empty. 
     */
    inline bool empty() const {
        return map.empty();
    }

    /**
     * Gets the value that corresponds to the given key. This method assumes that the 
     * map contains the provided key. 
     * @param key the key
     */
    inline V& get(K key) {
        return map[key].value;
    }

    /**
     * Gets the value that corresponds to the given key, or the default value, if
     * the map does not contain the key
     * @param key the key
     * @param backup the value to return if the key is not contained in the map
     */
    inline V getOrDefault(K key, const V&& backup) const {
        auto it = map.find(key);
        if (it != map.end()) {

            return it->second.value;
        }
        return backup;
    }

    /**
     * Removes the key from the map, if the map contains the key
     * @param key the key
     * @return whether the map contained the key
     */
    inline bool remove(K key) {
        if (contains(key)) {
            removeEntry(key);

            return true;
        }
        return false;
    }

    /**
     * Inserts the key-value pair into the map
     * @param key the key
     * @param value the corresponding value
     * @return whether an entry had to be overwritten in the map
     */
    inline bool insert(K key, const V& value) {
        if (!contains(key)) {
            if (shouldRemove()) {
                removeEldestEntry();
            }
            addEntry(key, value);
            return false;
        } else {
            replaceEntry(key, value);
            return true;
        }
    }

    /**
     *  Returns a read/write iterator that points to the first element in the
     *  map
     */
    inline Iterator begin() {
        return
        {
            keys, keys.begin()
        };
    }

    /**
     *  Returns a read/write iterator that points one past the last element in
     *  the map.
     */
    inline Iterator end() {
        return
        {
            keys, keys.end()
        };
    }

    /**
     *  Returns a read/write iterator that points to the first element in the
     *  map
     */
    inline ConstIterator begin()const {
        return
        {
            keys, keys.begin()
        };
    }

    /**
     *  Returns a read/write iterator that points one past the last element in
     *  the map.
     */
    inline ConstIterator end() const {
        return
        {
            keys, keys.end()
        };
    }



private:

    inline void removeEntry(K key) {
        keys.erase(map[key].iterator);
        map.erase(key);
    }

    inline void replaceEntry(K key, const V& value) {
        keys.erase(map[key].iterator);
        addEntry(key, value);
    }

    inline void addEntry(K key, const V& value) {
        keys.push_front(key);
        map[key] = {value, keys.begin()};
    }

    inline void removeEldestEntry() {
        K key = keys.back();
        keys.pop_back();
        map.erase(key);
    }

    inline bool shouldRemove() {
        return map.size() >= MAX_SIZE;
    }

    const int MAX_SIZE;
    std::unordered_map<K, Entry> map;
    std::list<K> keys;

    // <editor-fold defaultstate="collapsed" desc="Range-based For Loop Iterators">

    class Iterator {
        std::list<K> &data;
        typename std::list<K>::iterator position;
    public:

        Iterator(std::list<K> &data, typename std::list<K>::iterator position) :
        data(data), position(position) {
        }

        inline K& operator*() {
            return *position;
        }

        inline Iterator& operator++() {
            std::advance(position, 1);
            return *this;
        }

        inline bool operator!=(const Iterator& it) const {
            return position != it.position;
        }
    };

    class ConstIterator {
        const std::list<K> &data;
        typename std::list<K>::iterator position;
    public:

        ConstIterator(std::list<K> &data, typename std::list<K>::iterator position) :
        data(data), position(position) {
        }

        inline const K& operator*() const {
            return *position;
        }

        ConstIterator& operator++() {
            std::advance(position, 1);
            return *this;
        }

        bool operator!=(const ConstIterator& it) const {
            return position != it.position;
        }
    };
    // </editor-fold>

};

#endif	/* LINKEDHASHMAP_H */

